
import numpy as np
import matplotlib.pyplot as plt
import openpyxl

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print("Hola, bienvenido a {0}".format(name))

def Comunicacion(sheet):
    points = (sheet['E2'].value + sheet['F2'].value + sheet['G2'].value + sheet['H2'].value + sheet['I2'].value + sheet['J2'].value)
    prom = points/6
    return prom

def Honestidad(sheet):
    points = (sheet['L2'].value + sheet['M2'].value + sheet['N2'].value)
    prom = points / 3
    return prom

def Espiritu_de_Equipo(sheet):
    points = (sheet['P2'].value + sheet['Q2'].value + sheet['R2'].value + sheet['S2'].value + sheet['T2'].value)
    prom = points / 5
    return prom

def Mejora_Continua(sheet):
    points = (sheet['V2'].value + sheet['W2'].value + sheet['X2'].value + sheet['Y2'].value + sheet['Z2'].value)
    prom = points / 5
    return prom

def Empatia(sheet):
    points = (sheet['AB2'].value + sheet['AC2'].value + sheet['AD2'].value)
    prom = points / 3
    return prom

def Libertad_y_Responsabilidad_Colectiva(sheet):
    points = (sheet['AF2'].value + sheet['AG2'].value + sheet['AH2'].value)
    prom = points / 3
    return prom

def Work_hard(sheet):
    points = (sheet['AJ2'].value + sheet['AK2'].value + sheet['AL2'].value + sheet['AM2'].value + sheet['AN2'].value + sheet['AO2'].value)
    prom = points / 6
    return prom

def Colaboracion(sheet):
    points = (sheet['AQ2'].value + sheet['AR2'].value + sheet['AS2'].value + sheet['AT2'].value)
    prom = points / 4
    return prom

def Horizontalidad(sheet):
    points = (sheet['AV2'].value + sheet['AW2'].value + sheet['AX2'].value + sheet['AY2'].value)
    prom = points / 4
    return prom

# Importar Excel de Local
def Import_Excel():
    excel_document = openpyxl.load_workbook('Encuesta360.xlsx')
    # print(excel_document.get_sheet_names()) # Muestra todos los nombres de las paginas
    sheet = excel_document.get_sheet_by_name('Respuestas de formulario 1')
    return sheet

#Calcula los promedios de cada atributo
def Promedios(sheet):
    promedios = [Comunicacion(sheet), Honestidad(sheet), Espiritu_de_Equipo(sheet), Mejora_Continua(sheet), Empatia(sheet), Libertad_y_Responsabilidad_Colectiva(sheet), Work_hard(sheet), Colaboracion(sheet), Horizontalidad(sheet)]
    print('Comunicacion:', Comunicacion(sheet))
    print('Honestidad:', Honestidad(sheet))
    print('Espiritu de Equipo:', Espiritu_de_Equipo(sheet))
    print('Mejora Continua:', Mejora_Continua(sheet))
    print('Empatia:', Empatia(sheet))
    print('Libertad y Responsabilidad Colectiva:', Libertad_y_Responsabilidad_Colectiva(sheet))
    print('Work hard:', Work_hard(sheet))
    print('Colaboracion:', Colaboracion(sheet))
    print('Horizontalidad:', Horizontalidad(sheet))
    print(promedios)
    return promedios

def Diagrama(values):
    ## Creacion de un conjunto de datos aleatorio
    angles = np.linspace(0, 1 * np.pi, 9, endpoint=False)
    # Se repite el primer valor para cerrar el grafico
    angles = np.concatenate((angles, [angles[0]]))
    values = np.concatenate((values, [values[0]]))
    labels = ['Comunicacion', 'Honestidad', 'Espiritu de Equipo', 'Mejora Continua', 'Empatia', 'Libertad y Responsabilidad Colectiva', 'Work hard - Play hard', 'Colaboracion', 'Horizontalidad']
    # Representacion del mapa de calor
    plt.polar(angles, values, 'o-', linewidth=2)
    plt.fill(angles, values, alpha=0.25)
    plt.thetagrids(angles * 180 / np.pi, labels)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('Encuestas 360 GarageLabs')
    sheet = Import_Excel()
    promedios = Promedios(sheet)
    Diagrama(promedios)